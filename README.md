# README #

# Ça Graille ? #

Il s'agit d'une application permettant de rechercher tous les restaurants par ville. 
De plus, il est possible d'obtenir différentes informations telles que le nom, l'adresse et la note du restaurant sélectionné(note donnée par les utilisateurs).
L'application possède une fonctionnalité qui permet à l'utilisateur de choisir aléatoirement un restaurant dans une ville donnée lorsque celui-ci n'arrive pas à se décider sur quelle restaurant choisir.

# Informations supplémentaires #
API utilisé : Foursquare (https://fr.foursquare.com/) 
Cette API ne fournit pas toutes les informations sur les restaurants Français, ce qui entraine parfois des résultats vides ou "null".

# Développé avec Android Studio #
Version Android Studio : 3.0.1

*Testé sur :* 

 - Émulateur Android : Nexus 5X API 27 x86
 
 - Appareil physique : Samsung Galaxy S7 API 24

# Développeur #

 - Mathieu ALAIN (alain.mathieu1996@gmail.com)
 - Pierre MIGNOT (pierre.mignot49@gmail.com)

![Ça Graille ?](https://image.noelshack.com/fichiers/2018/04/4/1516887787-ic-cagraille.png)
	

