package com.example.evzparadise.agraille;


import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class FoursquareVenueUnitTest {

    @Test
    public void testDisplayFoursquareVenue() {
        FoursquareLocation location = new FoursquareLocation("10 Rue du Jardin", 45000, 4500 , 500, "Paris","75000");
        FoursquareContact contact = new FoursquareContact("02 51 43 84 52");
        FoursquareVenue venue = new FoursquareVenue("ab155aza","Les Pirates", 10, location, contact);
        String actual = venue.displayInformations();
        String expected = "The venue Les Pirates has a rating of 10.0 and is located at 10 Rue du Jardin 75000 Paris";
        assertEquals(expected, actual);
    }

}
