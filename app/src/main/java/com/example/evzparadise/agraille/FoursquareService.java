package com.example.evzparadise.agraille;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface FoursquareService {



    // A request to search for nearby coffee shop recommendations via the Foursquare API.
    @GET("venues/search?v=20161101")
    Call<FoursquareJSON> search(@Query("client_id") String clientID,
                                @Query("client_secret") String clientSecret,
                                @Query("near") String near,
                                @Query("query") String query,
                                @Query("limit") int limit);

}
