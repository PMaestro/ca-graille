package com.example.evzparadise.agraille;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.evzparadise.agraille.fragment.RandomFragment;

import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends Activity {

    private String foursquareClientID = "ZLAYCEY3ESV2SHTBFDCRIYBDTLWEBD5JEP225KPQF1KWLP2G";
    private String foursquareClientSecret = "LQM14LPU5IJY3NR2BWU3B3HSTNGWSETUPO0RV1KMVF12JRCP";
    private FragmentManager fragmentManager;
    private String city_name ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Button search_button = findViewById(R.id.search_button);
        final Button random_button = findViewById(R.id.random_button);
        final FrameLayout main_content = findViewById(R.id.main_content);
        final EditText c_name = findViewById (R.id.city_name);

        fragmentManager = getFragmentManager();
        search_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                city_name = c_name.getText().toString();
                Intent i = new Intent(MainActivity.this, DisplayActivity.class);
                i.putExtra("city_name",city_name);
                startActivity(i);
            }
        });

        random_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                city_name = c_name.getText().toString();
                fragmentManager.beginTransaction().replace(R.id.main_content, new RandomFragment()).commit();
                randomRestaurant(city_name);

            }
        });
    }

    public void randomRestaurant(String city) {
        String establishmentType = "restaurant";

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.foursquare.com/v2/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        final FoursquareService foursquare = retrofit.create(FoursquareService.class);

        Call<FoursquareJSON> restaurantsCall = foursquare.search(foursquareClientID,foursquareClientSecret,city,establishmentType,50);
        restaurantsCall.enqueue(new Callback<FoursquareJSON>() {
            @Override
            public void onResponse(Call<FoursquareJSON> call, Response<FoursquareJSON> response) {
                if (response.isSuccessful()) {
                    FoursquareJSON fjson = response.body();
                    FoursquareResponse fr = fjson.response;
                    Random rand = new Random();
                    int i = rand.nextInt(fr.venues.size());
                    FoursquareVenue restaurant = fr.venues.get(i);
                    TextView nameTextView = (TextView) findViewById(R.id.random_restaurant);
                    TextView addressTextView = (TextView) findViewById(R.id.random_address);
                    TextView phoneTextView = (TextView) findViewById(R.id.random_phone);
                    nameTextView.setText(restaurant.name);
                    addressTextView.setText(restaurant.getLocation().getAddress() + " " + restaurant.getLocation().getPostalCode() + " " + restaurant.getLocation().getCity());
                    phoneTextView.setText(restaurant.getContact().getFormattedPhone());
                } else {
                    Toast.makeText(MainActivity.this, "Error will calling the API - Enter a valid city name", Toast.LENGTH_LONG).show();
                    Log.d("Error", response.errorBody().toString() );
                }
            }

            @Override
            public void onFailure(Call<FoursquareJSON> call, Throwable t) {
                Toast.makeText(MainActivity.this, "OOOPSSS! Something went wrong", Toast.LENGTH_LONG).show();
                Log.d("Error", t.getMessage());
            }
        });
    }

}