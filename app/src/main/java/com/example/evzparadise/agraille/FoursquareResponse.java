package com.example.evzparadise.agraille;

import java.util.ArrayList;
import java.util.List;

public class FoursquareResponse {

    // A group object within the response.
    FoursquareGroup group;
    List<FoursquareVenue> venues = new ArrayList<>();

}