package com.example.evzparadise.agraille;

public class FoursquareLocation {

    // The address of the location.
    String address;

    // The latitude of the location.
    double lat;

    // The longitude of the location.
    double lng;

    // The distance of the location, calculated from the specified location.
    int distance;

    String city;

    String postalCode;

    public FoursquareLocation(String address, double lat, double lng, int distance, String city, String postalCode) {
        this.address = address;
        this.lat = lat;
        this.lng = lng;
        this.distance = distance;
        this.city = city;
        this.postalCode = postalCode;
    }


    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }
}