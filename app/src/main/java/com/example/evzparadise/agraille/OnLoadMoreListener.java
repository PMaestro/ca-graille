package com.example.evzparadise.agraille;

public interface OnLoadMoreListener {
    void onLoadMore();
}