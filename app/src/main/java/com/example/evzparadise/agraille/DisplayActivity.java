package com.example.evzparadise.agraille;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DisplayActivity extends AppCompatActivity {

    private List<FoursquareVenue> restaurants;
    private RestaurantsAdapter restaurantsAdapter;
    private String foursquareClientID = "ZLAYCEY3ESV2SHTBFDCRIYBDTLWEBD5JEP225KPQF1KWLP2G";
    private String foursquareClientSecret = "LQM14LPU5IJY3NR2BWU3B3HSTNGWSETUPO0RV1KMVF12JRCP";
    private int limit = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);

        restaurants = new ArrayList<>();
        String city_name = getIntent().getStringExtra("city_name");
        loadNext(city_name);


    }


    public void loadNext(String city) {
        String establishmentType = "restaurant";

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.foursquare.com/v2/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        FoursquareService foursquare = retrofit.create(FoursquareService.class);

        Call<FoursquareJSON> restaurantsCall = foursquare.search(foursquareClientID,foursquareClientSecret,city,establishmentType,limit);
        restaurantsCall.enqueue(new Callback<FoursquareJSON>() {
            @Override
            public void onResponse(Call<FoursquareJSON> call, Response<FoursquareJSON> response) {
                if (response.isSuccessful()) {
                    FoursquareJSON fjson = response.body();
                    FoursquareResponse fr = fjson.response;
                    if(restaurants.size() == fr.venues.size()){
                        Toast.makeText(DisplayActivity.this, "All data have been loaded", Toast.LENGTH_LONG).show();
                    }else{
                        for(int i = restaurants.size(); i < fr.venues.size(); i++){
                            restaurants.add(fr.venues.get(i));
                        }
                        if(limit == 10) {
                            final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
                            recyclerView.setLayoutManager(new LinearLayoutManager(DisplayActivity.this));
                            restaurantsAdapter = new RestaurantsAdapter(recyclerView, restaurants, DisplayActivity.this);
                            restaurantsAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                                @Override
                                public void onLoadMore() {
                                    limit += 10;
                                    loadNext("Paris");
                                }
                            });
                            recyclerView.setAdapter(restaurantsAdapter);
                        }else{
                            restaurantsAdapter.notifyDataSetChanged();
                            restaurantsAdapter.setLoaded();
                        }
                    }
                } else {
                    Toast.makeText(DisplayActivity.this, "Error will calling the API - Enter a valid city name", Toast.LENGTH_LONG).show();
                    Log.d("Error", response.errorBody().toString() );
                }
            }

            @Override
            public void onFailure(Call<FoursquareJSON> call, Throwable t) {
                Toast.makeText(DisplayActivity.this, "OOOPSSS! Something went wrong", Toast.LENGTH_LONG).show();
                Log.d("Error", t.getMessage());
            }
        });
    }

}
