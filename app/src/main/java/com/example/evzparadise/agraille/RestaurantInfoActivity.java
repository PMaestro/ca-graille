package com.example.evzparadise.agraille;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.List;

public class RestaurantInfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_info);
        Intent intent = getIntent();
        String name = intent.getStringExtra("name");
        String address = intent.getStringExtra("address");
        String phone = intent.getStringExtra("phone");
        TextView nameTextView = findViewById(R.id.name);
        TextView addressTextView = findViewById(R.id.address);
        TextView phoneTextView = findViewById(R.id.phone);
        nameTextView.setText(name);
        addressTextView.setText(address);
        phoneTextView.setText(phone);
    }
}
