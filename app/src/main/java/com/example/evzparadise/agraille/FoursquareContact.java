package com.example.evzparadise.agraille;

public class FoursquareContact {

    String formattedPhone;

    public FoursquareContact(String formattedPhone) {
        this.formattedPhone = formattedPhone;
    }

    public String getFormattedPhone() {
        return formattedPhone;
    }

    public void setFormattedPhone(String formattedPhone) {
        this.formattedPhone = formattedPhone;
    }
}