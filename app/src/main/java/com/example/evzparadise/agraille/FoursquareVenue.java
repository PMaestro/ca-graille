package com.example.evzparadise.agraille;

public class FoursquareVenue {

    // The ID of the venue.
    String id;

    // The name of the venue.
    String name;

    // The rating of the venue, if available.
    double rating;

    // A location object within the venue.
    FoursquareLocation location;

    FoursquareContact contact;


    public FoursquareVenue(String id, String name, double rating, FoursquareLocation location, FoursquareContact contact) {
        this.id = id;
        this.name = name;
        this.rating = rating;
        this.location = location;
        this.contact = contact;
    }

    public FoursquareContact getContact() {
        return contact;
    }

    public void setContact(FoursquareContact contact) {
        this.contact = contact;
    }

    public java.lang.String getId() {
        return id;
    }

    public void setId(java.lang.String id) {
        this.id = id;
    }

    public java.lang.String getName() {
        return name;
    }

    public void setName(java.lang.String name) {
        this.name = name;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public FoursquareLocation getLocation() {
        return location;
    }

    public void setLocation(FoursquareLocation location) {
        this.location = location;
    }

    public String displayInformations(){
        return "The venue " + this.name + " has a rating of " + rating + " and is located at " + location.getAddress() + " " + location.getPostalCode() + " " + location.getCity();
    }
}